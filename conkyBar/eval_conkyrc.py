# coding: utf-8

import re
import subprocess

CACHE = {}


def clear_cache():
    for key in list(CACHE.keys()):
        del CACHE[key]


def exec_cmd(cmd):
    if cmd not in CACHE:
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        CACHE[cmd] = process.communicate()[0].decode('utf-8').strip()
    return CACHE[cmd]


def get_meminfo(key):
    meminfo = exec_cmd('cat /proc/meminfo')
    for line in meminfo.splitlines():
        if line.startswith(key + ': '):
            return int(line.split()[1].strip())
    return 0


def kb_to_pretty(value):
    if value < 1024:
        return f'{round(value, 2):.2f}KiB'
    elif 1024 <= value < 1024**2:
        return f'{round(value/1024, 2):.2f}MiB'
    elif 1024**2 <= value < 1024**3:
        return f'{round(value/1024**2, 2):.2f}GiB'
    else:
        return f'{round(value/1024**3, 2):.2f}TiB'

def eval_conkyrc(conkyrc):
    conkyrc = conkyrc or ''
    text = conkyrc.split('conky.text = [[')[-1].split(']]')[0].strip()
    clear_cache()
    
    pattern = re.compile(r'\${[^{}]+}')
    match_start = 0
    while True:
        match = pattern.search(text, match_start)
        if not match:
            break
        substr = text[match.start():match.end()]
        command = substr[2:-1]
        
        if command.startswith('offset '):
            offset_int = int(command.split()[1])
            value = ' ' * int(round(offset_int / 5))
        elif command.startswith('texeci ') or command.startswith('execi '):
            cmd = ' '.join(command.split()[2:])
            value = exec_cmd(cmd)
        elif command.startswith('cpu '):
            cpu_name = command.split()[1]
            value = exec_cmd('grep \'' + cpu_name + ' \' /proc/stat | awk \'{usage=($2+$4)*100/($2+$4+$5)} END {print usage }\'')
            value = str(int(round(float(value))))
        elif command == 'mem':
            mem_total = get_meminfo('MemTotal')
            mem_free = get_meminfo('MemFree')
            # buffers = get_meminfo('Buffers')
            cached = get_meminfo('Cached')
            # s_reclaimable = get_meminfo('SReclaimable')
            # s_hmem = get_meminfo('Shmem')
            mem_used = (mem_total - mem_free) - cached
            value = kb_to_pretty(mem_used)
        elif command == 'memmax':
            mem_total = get_meminfo('MemTotal')
            value = kb_to_pretty(mem_total)
        elif command == 'swap':
            swap_total = get_meminfo('SwapTotal')
            swap_free = get_meminfo('SwapFree')
            swap_used = swap_total - swap_free
            value = kb_to_pretty(swap_used)
        elif command == 'swapmax':
            swap_total = get_meminfo('SwapTotal')
            value = kb_to_pretty(swap_total)
        elif command.startswith('diskio '):
            disk_name = command.split()[1]
            value = ''  # TODO
        else:
            value = ''
        
        text = text[:match.start()] + value + text[match.end():]
        match_start = match.start() + len(value)
    
    return text
