#!/usr/bin/env python3
# coding: utf-8

import os, argparse
import importlib.resources

from .configmanager import CONFIG_MANAGER
from .build_conkyrc import build_conkyrc
from .eval_conkyrc import eval_conkyrc

def main():
    # get path to app dir
    path = os.path.dirname(os.path.abspath(__file__))

    # init config
    with importlib.resources.path(__package__, 'config_default.cfg') as default_conf_path:
        CONFIG_MANAGER.loadConfig(default_conf_path)

    # get config file path - config file in same folder has higher priority
    conf_file = "config.cfg"
    # config in same folder as conkyBar.sh (../)
    if os.path.isfile(os.path.join(path, '..', conf_file)):
        conf_path = os.path.join(path, '..', conf_file)
    # config in ~/.config/conkybar folder
    else:
        try:
            import appdirs
            app_config_dir = appdirs.user_config_dir('conkybar')
        except:
            app_config_dir = os.path.join(os.path.expanduser("~"), '.config', 'conkybar')
        conf_path = os.path.join(app_config_dir, conf_file)

    # load user config
    if not os.path.isfile(conf_path):
        print("ERROR: config file not found in: %s" % (conf_path,))
    else:
        CONFIG_MANAGER.loadConfig(conf_path)

    # parse commandline args
    parser = argparse.ArgumentParser(description='ConkyBar')
    parser.add_argument('--buildconkyrc', '-c', default=None,
                        help='Builds new conkyrc in on given path and exits')
    parser.add_argument('--print', '-p', action='store_true',
                        help='Print information to terminal instead')
    parser.add_argument('--disable_section_system', action='store_false')
    parser.add_argument('--disable_section_cpu', action='store_false')
    parser.add_argument('--disable_section_mem', action='store_false')
    parser.add_argument('--disable_section_swap', action='store_false')
    parser.add_argument('--disable_section_gpu', action='store_false')
    parser.add_argument('--disable_section_net', action='store_false')
    parser.add_argument('--disable_section_io', action='store_false')
    parser.add_argument('--disable_section_autoruns', action='store_false')
    args = parser.parse_args()

    # do
    conkyrc = build_conkyrc(
        path=args.buildconkyrc, 
        section_system=args.disable_section_system and CONFIG_MANAGER.getBoolean('Sections', 'System'), 
        section_cpu=args.disable_section_cpu and CONFIG_MANAGER.getBoolean('Sections', 'CPU'), 
        section_mem=args.disable_section_mem and CONFIG_MANAGER.getBoolean('Sections', 'Mem'), 
        section_swap=args.disable_section_swap and CONFIG_MANAGER.getBoolean('Sections', 'Swap'), 
        section_gpu=args.disable_section_gpu and CONFIG_MANAGER.getBoolean('Sections', 'GPU'), 
        section_net=args.disable_section_net and CONFIG_MANAGER.getBoolean('Sections', 'Net'), 
        section_io=args.disable_section_io and CONFIG_MANAGER.getBoolean('Sections', 'IO'), 
        section_autoruns=args.disable_section_autoruns,
    )
    if args.print:
        print(eval_conkyrc(conkyrc))        

main()
