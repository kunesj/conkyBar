#!/bin/bash
#
# Run this script to start or stop conkyBar



#
# start conkyBar
#
start_cp()
{
	echo 'starting' \
	&& folderPath=$(dirname $0) \
	&& cd "$folderPath" \
	&& python3 -m conkyBar -c conkyBarRc \
	&& conky -d -c conkyBarRc
}

#
# stop conkyBar
#
stop_cp()
{
	echo 'stopping'
	PID=$(ps -C conky a | grep 'conkyBar' | grep '\-d' | awk {'print $1'})
	if [ -z "$PID" ]
	then
		echo "No running ConkyBar instances found"
	else
		kill -9 $PID
	fi
}

#
# print the manual
#
echo_manual()
{
	echo '--- Usage ---'
	echo 'Starting conkyBar: ./conkyBar.sh start'
	echo 'Stopping conkyBar: ./conkyBar.sh stop'
	echo 'Restarting conkyBar: ./conkyBar.sh restart'
}

if [ $1 ]; then
	if [ $1 = "start" ]; then
		start_cp
	elif [ $1 = "stop" ]; then
		stop_cp
	elif [ $1 = "restart" ]; then
		echo 'restarting'
		stop_cp
		start_cp
	else
		echo_manual
	fi
else
	echo_manual
fi

exit 0
