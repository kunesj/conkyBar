# ! /usr/bin/python
# coding: utf-8

import imaplib
import argparse

parser = argparse.ArgumentParser(
    description='Prints number of unread emails (IMAP)'
)
parser.add_argument(
    '--link'
    )
parser.add_argument(
    '--port',
    default='993'
    )
parser.add_argument(
    '--username'
    )
parser.add_argument(
    '--password'
    )

try:
    args = parser.parse_args()

    #print "imap request: "+str(args.link)+", "+str(args.port)+", "+str(args.username)

    obj = imaplib.IMAP4_SSL(args.link, args.port)
    obj.login(args.username, args.password)
    obj.select()

    print len(obj.search(None, 'UnSeen')[1][0].split())
except:
    print '-'

